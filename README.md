# go cassandra

> Author *`michaelspring123@web.de`*
> > Creation date *`2021-08-10`*
> > > version *`alpha`*


```mermaid
graph LR
    id1(testsetup)-->id2(create data)-->id3(mess around)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```


# initialize the project

```console
Last login: Sun Nov 14 18:34:38 on ttys000
asdf@asdfs-MBP ~ % cd go-cassandra
asdf@asdfs-MBP go-cassandra % go mod init Users/asdf/go-cassandra/gotest
go: creating new go.mod: module Users/asdf/go-cassandra/gotest
asdf@asdfs-MBP go-cassandra % cat go.mod
module Users/asdf/go-cassandra/gotest

go 1.17
asdf@asdfs-MBP go-cassandra % 
```



